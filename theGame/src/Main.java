import java.util.Scanner;

/**
 * Description : You need to implement a card game which can be played in 2,3 or 4 players and it`s played with a 24 cards deck.
 * The cards are four colours :T,R,S and M and their values are :11,10,4,3,2 and 9 (the 9 card is 0 points).
 * The players each get 6 cards if there are 4 players, 8 cards if there are 3 players and 12 cards if there are 2 players.
 * The game is played like this: they each put the most valuable card on the table and the player that had the most valuable card takes all of the cards from the table.
 * If two players put on the table cards with the same value then the one that put first a card down will take them and start the next round.
 * How many players and who starts is chosen by the user.
 * You can only use the priority queue container implemented by you for the game.

 * Exemple:
 *  we have 4 players
 * o J1: (11, T) (11, R) (4, T) (3, T) (9, R) (9, M)
 * o J2: (10, S) (10, R) (3, R) (3, M) (2, T) (9, S)
 * o J3: (11, S) (4, R) (4, S) (4, M) (2, R) (2, S)
 * o J4: (11, M) (10, T) (10, M) (3, S) (2, M) (9, T)
 *  Player 3 starts
 * o Tura 1: (11, S) (11, M) (11, T) (10, S) – jucătorul 3 takes the cards.
 * o Tura 2: (4, R) (10, T) (11, R) (10, R) – jucătorul 1 takes the cards
 * o Tura 3: (4, T) (3, R) (4, S) (10, M) – jucătorul 4 takes the cards
 * o Tura 4: (3, S) (3, T) (3, M) (4, M) – jucătorul 3 takes the cards
 * o Tura 5: (2, R) (2, M) (9, R) (2, T) – jucătorul 3 takes the cards
 * o Tura 6: (2, S), (9, T), (9, M), (9, S) – jucătorul 3 takes the cards
 *  Scores
 * o Jucătorul 1: 4+10+11+10 = 35
 * o Jucătorul 2: 0
 * o Jucătorul 3: 11+11+11+10+3+3+3+4+2+2+2+2 = 64
 * o Jucătorul 4: 4+3+4+10 = 21
 *  Jucătorul 3 has won
 */

public class Main {

    public static void main(String[] args) {
        System.out.println("Cati jucatori ?");
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        System.out.println("cine incepe?");
        int primJucator=scanner.nextInt();
        theGame theGame =new theGame();
        theGame.joc(n,primJucator);
    }

}
