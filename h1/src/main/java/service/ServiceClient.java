package service;

import domain.Client;
import domain.Inchirieri;
import domain.Rentals;
import repository.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceClient {

    private Repository<Long, Client> repositoryClient;
    private Repository<Inchirieri, Rentals> rentalsRepository;

    public ServiceClient(Repository<Long, Client> repositoryClient, Repository<Inchirieri, Rentals> rentalsRepository) {
        this.repositoryClient = repositoryClient;
        this.rentalsRepository = rentalsRepository;
    }

    public void saveClient(Client client) {

        repositoryClient.save(client);
    }


    public Set<Client> getAllClients() {
        Iterable<Client> clients = repositoryClient.findAll();
        return StreamSupport.stream(clients.spliterator(), false).collect(Collectors.toSet());
    }


    public void delete(String nume) {
        List<Client> list = StreamSupport.stream(repositoryClient.findAll().spliterator(), false)
                .filter(client -> client.getName().equals(nume))
                .collect(Collectors.toList());

        //   Optional<Long> optionalInteger = Optional.ofNullable(list.stream().count());
        // optionalInteger.orElseThrow(() -> new IllegalArgumentException("client not found"));
        if (list.stream().count() == 0) {
            throw new IllegalArgumentException("client not found");
        }
        List<Rentals> list1 = StreamSupport.stream(rentalsRepository.findAll().spliterator(), false)
                .filter(rentals -> rentals.getId().getIdClient().equals(list.get(0).getId().toString()))
                .collect(Collectors.toList());
        rentalsRepository.delete(list1.get(0).getId());
        repositoryClient.delete(list.get(0).getId());

    }

    public Client findOneClient(String name) {
        List<Client> clients = StreamSupport.stream(repositoryClient.findAll().spliterator(), false)
                .filter(client -> client.getName().contains(name))
                .collect(Collectors.toList());
        if (clients.stream().count() == 0) {
            throw new IllegalArgumentException("client not found");
        }
        return clients.get(0);
    }

    public void update(String numeVechi, Client client) {
        List<Client> list = StreamSupport.stream(repositoryClient.findAll().spliterator(), false)
                .filter(client1 -> client1.getName().equals(numeVechi))
                .collect(Collectors.toList());
        //   Optional<Long> optionalInteger = Optional.ofNullable(list.stream().count());
        // optionalInteger.orElseThrow(() -> new IllegalArgumentException("client not found"));
        if (list.stream().count() == 0) {
            throw new IllegalArgumentException("client not found");
        }

        Long id = list.get(0).getId();
        client.setId(id);
        repositoryClient.update(client);


    }
}
