package ui;

import domain.Client;
import domain.Movie;
import domain.Rentals;
import service.ServiceClient;
import service.ServiceMovie;
import service.ServiceRental;

import java.util.Scanner;
import java.util.Set;


public class Consola {
    private ServiceMovie serviceMovie;
    private ServiceClient serviceClient;
    private ServiceRental serviceRental;
    private Scanner scanner;

    public Consola(ServiceMovie serviceMovie, ServiceClient serviceClient, ServiceRental serviceRental) {
        this.serviceMovie = serviceMovie;
        this.serviceClient = serviceClient;
        this.serviceRental = serviceRental;

    }

    public void runMenu() {
        while (true) {
            printOptions();
            this.scanner = new Scanner(System.in);
            String option = scanner.next();
            switch (option) {
                case "1":
                    addMovie();
                    break;
                case "2":
                    addClient();
                    break;
                case "3":
                    showAllMovies();
                    break;
                case "4":
                    showAllClients();
                    break;
                case "5":
                    showRentMovies();
                    break;
                case "6":
                    rentMovie();
                    break;
                case "7":
                    deleteClient();
                    break;
                case "8":
                    deleteMovie();
                    break;
                case "9":
                    findOneMovie();
                    break;
                case "10":
                    findOneClient();
                    break;
                case "11":
                    updateMovie();
                    break;
                case "12":
                    updateClient();
                    break;
                case "13":
                    deleteRentals();
                    break;
                case "14":
                    filtru();
                    break;
                case "15":
                    filtru2();
                    break;
                case "16":
                    priceSearch();
                    break;
                case "x":
                    break;
            }
        }
    }

    private void priceSearch() {
        System.out.println("insert the first price");
        int price1 = Integer.parseInt(scanner.next());
        System.out.println("insert the second price");
        int price2 = Integer.parseInt(scanner.next());

        serviceMovie.searchByPrice(price1, price2).stream().forEach(movie -> System.out.println(movie));


    }


    private void filtru2() {
        System.out.println("Insert the number of entries");
        int n = Integer.parseInt(scanner.next());
        System.out.println(serviceRental.filtru(n));


    }

    private void filtru() {

        serviceRental.filtruHelper().stream().forEach(System.out::println);
    }

    private void deleteRentals() {
        System.out.println("Insert id client");
        String idclient = scanner.next();
        System.out.println("Insert id movie");
        String idmovie = scanner.next();
        try {
            serviceRental.delete(idclient, idmovie);

        } catch (IllegalArgumentException e) {
            System.out.println("rental not found");
        }
    }

    private void rentMovie() {
        System.out.println("Insert the movie name ");
        String numeFilm = scanner.next();
        System.out.println("Insert the client name ");
        String numeClient = scanner.next();
        try {

            serviceRental.saveRental(numeFilm, numeClient);
        } catch (IllegalArgumentException e) {
            e.getMessage();
        }
    }


    private void deleteMovie() {
        System.out.println("Insert the movie name");
        String nume = scanner.next();
        try {
            serviceMovie.delete(nume);
        } catch (IllegalArgumentException E) {
            System.out.println("movie not found");
        }
    }

    private void deleteClient() {
        System.out.println("Insert The name of the Client: ");
        String name = scanner.next();
        try {
            serviceClient.delete(name);
        } catch (IllegalArgumentException e) {
            System.out.println("client not found ");
        }
    }

    private void addMovie() {

        System.out.println("Insert the Movie name: ");
        String nume = scanner.next();
        System.out.println("Insert the price of the Movie: ");
        int pret = scanner.nextInt();
        System.out.println("Insert movie genre: ");
        String gen = scanner.next();
        Movie filmNou = new Movie(nume, pret, gen);
        serviceMovie.saveMovie(filmNou);
    }

    private void addClient() {
        System.out.println("Insert the client name: ");
        String name = scanner.next();
        System.out.println("Insert his/her age: ");
        Integer age = scanner.nextInt();
        Client newClient = new Client(name, age);
        serviceClient.saveClient(newClient);
    }

    private void showAllMovies() {
        Set<Movie> movies = serviceMovie.getAllMovies();
        movies.stream().forEach(System.out::println);
    }

    private void showRentMovies() {
        Set<Rentals> rentals = serviceRental.getAllRentals();
        rentals.stream().forEach(System.out::println);

    }

    private void showAllClients() {
        Set<Client> clients = serviceClient.getAllClients();
        clients.stream().forEach(System.out::println);
    }


    private void findOneMovie() {
        System.out.println("Insert movie name");
        String name = scanner.next();

        try {
            System.out.println(serviceMovie.findOne(name));
        } catch (IllegalArgumentException e) {
            System.out.println("movie not found");
        }
    }

    private void findOneClient() {
        System.out.println("Insert client name");
        String name = scanner.next();
        try {
            System.out.println(serviceClient.findOneClient(name));
        } catch (IllegalArgumentException e) {
            System.out.println("client not found");
        }
    }

    private void updateMovie() {
        System.out.println("Insert the movie name");
        String numeVechi = scanner.next();

        System.out.println("Insert the Movie name: ");
        String nume = scanner.next();
        System.out.println("Insert the price of the Movie: ");
        int pret = scanner.nextInt();
        System.out.println("Insert movie genre: ");
        String gen = scanner.next();
        Movie movie = new Movie(nume, pret, gen);
        try {

            serviceMovie.update(numeVechi, movie);
        } catch (IllegalArgumentException e) {
            System.out.println("movie not found");
        }


    }

    private void updateClient() {
        System.out.println("Insert the client name");
        String numeVechi = scanner.next();
        System.out.println("Insert the client name: ");
        String name = scanner.next();
        System.out.println("Insert his/her age: ");
        Integer age = scanner.nextInt();
        Client newClient = new Client(name, age);
        try {


            serviceClient.update(numeVechi, newClient);
        } catch (IllegalArgumentException e) {
            System.out.println("client not found");
        }
    }


    private void printOptions() {
        System.out.println("1.Add a new Movie");
        System.out.println("2.Add a new Client");
        System.out.println("3.Show All Movies");
        System.out.println("4.Show All Clients");
        System.out.println("5.Show All Rent Movies");
        System.out.println("6.Rent a Movie ");
        System.out.println("7.Delete client");
        System.out.println("8.Delete movie");
        System.out.println("9.Find One Movie");
        System.out.println("10.Find Client");
        System.out.println("11.Update Movie");
        System.out.println("12.Update client");
        System.out.println("13.Delete rental");
        System.out.println("14. Clients renting the most expensive movie sorted by age");
        System.out.println("15 Top n most active clients");
        System.out.println("16 Search a movie in a price range");
        System.out.println("x. Exit!");
        System.out.println();
        System.out.print("Insert your option:");
        System.out.println();
    }
}
