package repository;

import domain.Inchirieri;
import domain.Rentals;
import validators.Validator;
import validators.ValidatorException;

import java.sql.*;
import java.util.*;


public class RentalsRepoSql extends InMemoryRepository<Inchirieri, Rentals> {


    private static final String URL = "jdbc:postgresql://localhost:5432" +
            "/movies";
    private static final String USERNAME = System.getProperty("user");
    private static final String PASSWORD = System.getProperty("password");

    public RentalsRepoSql(Validator<Rentals> validator) {
        super(validator);
    }


//    public List<String> filtru() {
//        List<String> filtru = super.filtru();
//        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
//                PASSWORD);
//             PreparedStatement statement = connection.prepareStatement(
//                     "select c.name,m.price\n" +
//                             "from clients c inner join rentals r on CAST (c.id AS VARCHAR)=r.idclient inner join movies m on r.idmovie = CAST(m.id AS VARCHAR) \n" +
//                             "order by price desc\n" +
//                             "limit 3");
//             ResultSet resultSet = statement.executeQuery()) {
//
//            while (resultSet.next()) {
//
//                String nameClient = resultSet.getString("name");
//
//
//                filtru.add(nameClient);
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//
//        }


//        return filtru;
//    }

    @Override
    public Optional<Rentals> save(Rentals entity) throws ValidatorException {

        Optional<Rentals> optional = super.save(entity);
        saveIntoDB(entity);
        if (optional.isPresent()) {
            return optional;
        }


        return Optional.empty();

    }

    @Override
    public Optional<Rentals> delete(Inchirieri inchirieri) {


        Optional<Rentals> optional = super.delete(inchirieri);
        deleteFromDB(inchirieri);


        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();


    }

    @Override
    public Optional<Rentals> update(Rentals rentals) throws ValidatorException {
        Optional<Rentals> optional = super.update(rentals);
        if (optional.isPresent()) {
            return optional;
        }

        updateDB(rentals);
        return Optional.empty();
    }


    public List<Rentals> findAll() {
        List<Rentals> rentals = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "select * from rentals");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {

                String nameMovie = resultSet.getString("idMovie");

                String nameClient = resultSet.getString("idClient");

                Rentals rentals1 = new Rentals();
                Inchirieri inchirieri = new Inchirieri(nameClient, nameMovie);
                rentals1.setId(inchirieri);

                rentals.add(rentals1);
            }

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return rentals;
    }

    private void saveIntoDB(Rentals rentals) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "insert into rentals (idclient, idmovie) " +
                             "values (?,?)");
        ) {
            statement.setString(1, rentals.getId().getIdClient());
            statement.setString(2, rentals.getId().getIdMovie());

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        }

    }


    private void updateDB(Rentals rental) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "update rentals set idmovie = ?,idclient=? where id= ?")
        ) {
            statement.setString(1, rental.getId().getIdMovie());
            statement.setString(2, rental.getId().getIdClient());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void deleteFromDB(Inchirieri inchirieri) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "delete from rentals where idclient = ? and idmovie= ?")
        ) {
            statement.setString(1, inchirieri.getIdClient());
            statement.setString(2, inchirieri.getIdMovie());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
