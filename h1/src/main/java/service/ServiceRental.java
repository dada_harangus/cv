package service;


import domain.Client;
import domain.Inchirieri;
import domain.Movie;
import domain.Rentals;

import repository.Repository;


import java.util.*;

import java.util.stream.Collectors;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;


public class ServiceRental {

    private Repository<Inchirieri, Rentals> repositoryRentals;
    private Repository<Long, Client> clientRepository;
    private Repository<Long, Movie> movieRepository;


    public ServiceRental(Repository<Inchirieri, Rentals> repositoryRentals, Repository<Long, Client> clientRepository, Repository<Long, Movie> movieRepository) {
        this.repositoryRentals = repositoryRentals;
        this.clientRepository = clientRepository;
        this.movieRepository = movieRepository;
    }

    /**
     * finds the most expensive movie and the clients that rented it
     */

    public Set<Client> filtruHelper() {


        Set<String> set = new HashSet<>();
        Set<Client> clientSet = new HashSet<>();
        Set<Movie> movieSet = new HashSet<>();
/**
 * add to a set all movies that are rented
 */


        repositoryRentals.findAll().forEach(rentals -> set.add(rentals.getId().getIdMovie()));
        movieRepository.findAll().forEach(movie -> {
            if (set.contains(movie.getId().toString())) {
                movieSet.add(movie);
            }
        });
//find the most expensive movie
        Movie movie = movieSet.stream()
                .reduce((p1, p2) -> p1.getPret() > p2.getPret() ? p1 : p2)
                .get();

        //find out which clients rented that movie
        List<Rentals> list = StreamSupport.stream(repositoryRentals.findAll().spliterator(), false)
                .filter(rentals -> rentals.getId().getIdMovie().equals(movie.getId().toString()))
                .collect(Collectors.toList());


        clientRepository.findAll().forEach(client -> {
            if (list.contains(client.getId().toString())) {
                clientSet.add(client);
            }
        });

///sort by age
        SortedSet<Client> set1 = new TreeSet<>(
                Comparator.comparing(Client::getAge));
        clientSet.stream()
                .forEach(client -> set1.add(client));


        return set1;
    }

    /**
     * saves a rental
     *
     * @param numeMovie  movie name
     * @param numeClient client name
     */
    public void saveRental(String numeMovie, String numeClient) throws IllegalArgumentException {


        List<Movie> list = StreamSupport.stream(movieRepository.findAll().spliterator(), false)
                .filter(movie -> movie.getName().equals(numeMovie))
                .collect(Collectors.toList());
        //      Optional<Long> optionalInteger = Optional.ofNullable(list.stream().count());
//        optionalInteger.orElseThrow(() -> new IllegalArgumentException("movie not found"));
        if (list.stream().count() == 0) {
            throw new IllegalArgumentException("movie not found");
        }

        String movie = list.get(0).getId().toString();

        List<Client> list1 = StreamSupport.stream(clientRepository.findAll().spliterator(), false)

                .filter(client -> client.getName().equals(numeClient))
                .collect(Collectors.toList());


        // Optional<Long> optional = Optional.ofNullable(list.stream().count());
        // optional.orElseThrow(() -> new IllegalArgumentException("client not found"));

        if (list.stream().count() == 0) {
            throw new IllegalArgumentException("client not found");
        }
        String client = list1.get(0).getId().toString();

        Inchirieri inchirieri = new Inchirieri(client, movie);
        Rentals rentals = new Rentals();
        rentals.setId(inchirieri);
        repositoryRentals.save(rentals);
    }


    /**
     * finds the most active clients
     *
     * @param n the number of entries the users wants
     * @return top n clients that rented the most movies
     */

    public Map filtru(int n) {
//collect is a map that groups the rentals list by clientid and the number of times that id is in the list
        Map<String, List<Rentals>> collect = StreamSupport.stream(repositoryRentals.findAll().spliterator(), false)
                .collect(Collectors.groupingBy(rentals -> rentals.getId().getIdClient()));


        Map<String, Integer> map = new HashMap<>();
// we take the client id and the number of entries and put it in a mapp
        collect.entrySet().stream()
                .forEach(stringListEntry -> map.put(stringListEntry.getKey(), stringListEntry.getValue().size()));

        Map<String, Integer> map2 = new HashMap<>();
//we take the client id and search for it in client list
        StreamSupport.stream(clientRepository.findAll().spliterator(), false)
                .forEach(client -> {

                    if (map.containsKey(client.getId().toString())) {
                        map2.put(client.getName(), map.get(client.getId().toString()).intValue());
                    }


                });
/**
 * we sort and limit the findings
 */
        final Map<String, Integer> mapSortata = map2.entrySet()
                .stream()
                .sorted((Map.Entry.<String, Integer>comparingByValue().reversed()))
                .limit(n)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));


        return mapSortata;


    }


    public Set<Rentals> getAllRentals() {
        Iterable<Rentals> movies = repositoryRentals.findAll();

        return StreamSupport.stream(movies.spliterator(), false).collect(Collectors.toSet());
    }

    public void delete(String idclient, String idmovie) {

        List<Rentals> list = StreamSupport.stream(repositoryRentals.findAll().spliterator(), false)
                .filter(rentals -> rentals.getId().getIdClient().equals(idclient))
                .filter(rentals -> rentals.getId().getIdMovie().equals(idmovie))
                .collect(Collectors.toList());
        //   Optional<Long> optionalInteger = Optional.ofNullable(list.stream().count());
        // optionalInteger.orElseThrow(() -> new IllegalArgumentException("rental not found"));

        if (list.stream().count() == 0) {
            throw new IllegalArgumentException("rental not found");
        }
        repositoryRentals.delete(list.get(0).getId());
    }


}
