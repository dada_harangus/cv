package repository;

import domain.Client;
import domain.Movie;
import validators.Validator;
import validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

public class ClientRepo extends InMemoryRepository<Long, Client> {

    private String client;

    public ClientRepo(Validator<Client> validator, String client) {
        super(validator);
        this.client = client;

        loadData();
    }

    private void loadData() {
        Path path = Paths.get(client);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long id = Long.valueOf(items.get(2));
                String nume = items.get(0);
                int varsta = Integer.parseInt(items.get((1)));

                Client client = new Client(nume, varsta);
                client.setId(id);

                try {
                    super.save(client);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Optional<Client> save(Client entity) throws ValidatorException {
        if (StreamSupport.stream(super.findAll().spliterator(), false).count() == 0) {
            entity.setId((long) 1);

        } else {
            Client client = StreamSupport.stream(super.findAll().spliterator(), false)
                    .reduce((p1, p2) -> p1.getId() > p2.getId() ? p1 : p2)
                    .get();
            entity.setId(client.getId() + 1);
        }
        Optional<Client> optional = super.save(entity);
        saveToFile(entity);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }

    private void saveToFile(Client entity) {
        Path path = Paths.get(client);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getName() + "," + entity.getAge() + "," + entity.getId());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void rewriteFile() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(client))) {
            for (Client entity : super.findAll()) {
                writer.write(entity.getName() + "," + entity.getAge() + "," + entity.getId());
                writer.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Optional<Client> update(Client client1) throws ValidatorException {

        Optional<Client> optional = super.update(client1);

        rewriteFile();


        if (optional.isPresent()) {
            return optional;
        }


        return Optional.empty();
    }


    @Override

    public Optional<Client> delete(Long id) {


        Optional<Client> optional = super.delete(id);

        rewriteFile();


        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }

}