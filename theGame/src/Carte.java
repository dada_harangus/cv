import java.util.ArrayList;
import java.util.List;

public class Carte {

    private String culoare;
    private int numar;

    public Carte(String culoare, int numar) {
        this.culoare = culoare;
        this.numar = numar;
    }

    public String getCuloare() {
        return culoare;
    }

    public void setCuloare(String culoare) {
        this.culoare = culoare;
    }

    public int getNumar() {
        return numar;
    }

    public void setNumar(int numar) {
        this.numar = numar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Carte)) return false;

        Carte carte = (Carte) o;

        if (getNumar() != carte.getNumar()) return false;
        return getCuloare() != null ? getCuloare().equals(carte.getCuloare()) : carte.getCuloare() == null;
    }

    @Override
    public int hashCode() {
        int result = getCuloare() != null ? getCuloare().hashCode() : 0;
        result = 31 * result + getNumar();
        return result;
    }

    @Override
    public String toString() {
        return "Carte{" +
                "culoare='" + culoare + '\'' +
                ", numar=" + numar +
                '}';
    }
}
