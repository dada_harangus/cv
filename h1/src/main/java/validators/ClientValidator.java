package validators;

import domain.Client;


import java.util.Optional;

public class ClientValidator implements Validator<Client> {


    @Override
    public void validate(Client entity) throws ValidatorException {

        Optional<Client> op = Optional.ofNullable(entity);

        op = op.filter(client -> client.getAge() < 100);
        op = op.filter(client -> client.getAge() > 0);
        op.orElseThrow(() -> new ValidatorException("The price is out of bound"));

    }

}