package repository;

import domain.Client;
import validators.Validator;
import validators.ValidatorException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientSqlRepo extends InMemoryRepository<Long, Client> {

    private static final String URL = "jdbc:postgresql://localhost:5432" +
            "/movies";
    private static final String USERNAME = System.getProperty("user");
    private static final String PASSWORD = System.getProperty("password");


    public ClientSqlRepo(Validator validator) {
        super(validator);
    }

    public Optional<Client> save(Client client) throws ValidatorException {
        Optional<Client> optional = super.save(client);
        saveToDb(client);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }

    public void saveToDb(Client client) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement("Insert into clients(name, age)" + "values(?,?)");) {
            statement.setString(1, client.getName());
            statement.setInt(2, client.getAge());
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public Optional<Client> delete(Long id) {
        Optional<Client> optional = super.delete((id));
        deleteFromDb(id);
        if (optional.isPresent()) {
            return optional;
        }
        return Optional.empty();
    }

    private void deleteFromDb(Long id) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "delete from clients where id = ?")
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public Optional update(Client client) {
        Optional<Client> optional = super.update(client);
        updateDb(client);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }

    private void updateDb(Client client) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             PreparedStatement statement = connection.prepareStatement("update clients set name = ?, age = ? where id=?")) {
            statement.setString(1, client.getName());
            statement.setInt(2, client.getAge());
            statement.setLong(3, client.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<Client> findAll() {
        List<Client> clients = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             PreparedStatement statement = connection.prepareStatement("select * from clients");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                Integer age = resultSet.getInt("age");

                Client client = new Client();
                client.setId(id);
                client.setName(name);
                client.setAge(age);

                clients.add(client);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return clients;
    }
}
