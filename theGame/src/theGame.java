import java.util.*;

import static java.lang.Integer.max;

public class theGame {

    /**
     * an Arraylist to hold the cards
     */
    List<Carte> listadeCarti = new ArrayList<>();


    /**
     * create the cards and put them in the list
     */
    private List<Carte> impartire() {
        Carte carte = new Carte("R", 11);
        Carte carte1 = new Carte("R", 10);
        Carte carte2 = new Carte("R", 4);
        Carte carte3 = new Carte("R", 3);
        Carte carte4 = new Carte("R", 2);
        Carte carte5 = new Carte("R", 9);
        Carte carte6 = new Carte("T", 11);
        Carte carte7 = new Carte("T", 10);
        Carte carte8 = new Carte("T", 4);
        Carte carte9 = new Carte("T", 3);
        Carte carte10 = new Carte("T", 2);
        Carte carte11 = new Carte("T", 9);
        Carte carte12 = new Carte("S", 11);
        Carte carte13 = new Carte("S", 10);
        Carte carte14 = new Carte("S", 4);
        Carte carte15 = new Carte("S", 3);
        Carte carte16 = new Carte("S", 2);
        Carte carte17 = new Carte("S", 9);
        Carte carte18 = new Carte("M", 11);
        Carte carte19 = new Carte("M", 10);
        Carte carte20 = new Carte("M", 4);
        Carte carte21 = new Carte("M", 3);
        Carte carte22 = new Carte("M", 2);
        Carte carte23 = new Carte("M", 9);
        listadeCarti.add(carte);
        listadeCarti.add(carte1);
        listadeCarti.add(carte2);
        listadeCarti.add(carte3);
        listadeCarti.add(carte4);
        listadeCarti.add(carte5);
        listadeCarti.add(carte6);
        listadeCarti.add(carte7);
        listadeCarti.add(carte8);
        listadeCarti.add(carte9);
        listadeCarti.add(carte10);
        listadeCarti.add(carte11);
        listadeCarti.add(carte12);
        listadeCarti.add(carte13);
        listadeCarti.add(carte14);
        listadeCarti.add(carte15);
        listadeCarti.add(carte16);
        listadeCarti.add(carte17);
        listadeCarti.add(carte18);
        listadeCarti.add(carte19);
        listadeCarti.add(carte20);
        listadeCarti.add(carte21);
        listadeCarti.add(carte22);
        listadeCarti.add(carte23);
        return listadeCarti;
    }

    /**
     * j1,j2,j3,j4 =initial cards for every player
     * coada pentru jucator1,2,3,4= Queues for holding the cards after each round
     * coada pentru joc= the queue where we play
     */


    CoadaPeVector j1 = new CoadaPeVector(6);
    CoadaPeVector j2 = new CoadaPeVector(6);
    CoadaPeVector j3 = new CoadaPeVector(6);
    CoadaPeVector j4 = new CoadaPeVector(6);
    CoadaPeVector coadaPentrujoc = new CoadaPeVector(2);
    CoadaPeVector coadaPentrujucator1 = new CoadaPeVector(4);
    CoadaPeVector coadaPentrujucator2 = new CoadaPeVector(4);
    CoadaPeVector coadaPentrujucator3 = new CoadaPeVector(4);
    CoadaPeVector coadaPentrujucator4 = new CoadaPeVector(4);
    Comparator.Comparatorcarti comparator = new Comparator.Comparatorcarti();
    Validator validator = new Validator();

    /**
     * calculate the sums for each player at the end of the game
     */


    private int calculateWinner(CoadaPeVector coada) {
        int sum = 0;
        while (coada.size() != 0) {
            int i = coada.pool().getNumar();
            if (i == 9) {
                i = 0;
            } else {
                sum += i;
            }
        }
        return sum;
    }

    /**
     * if the winner of the round is n the players queue add the cards
     *
     * @param n which player has won the round
     * @return which player should start the next round
     */

    private int decision(int n) {
        if (n == 2) {
            while (coadaPentrujoc.size() != 0) {
                coadaPentrujucator2.add(coadaPentrujoc.pool());
                n = 2;
                System.out.println("Jucatorul 2 a luat cartile");
            }
        } else if (n == 3) {
            while (coadaPentrujoc.size() != 0) {
                coadaPentrujucator3.add(coadaPentrujoc.pool());
                n = 3;
                System.out.println("Jucatorul 3 a luat cartile");
            }

        } else if (n == 4) {
            while (coadaPentrujoc.size() != 0) {
                coadaPentrujucator4.add(coadaPentrujoc.pool());
                n = 4;
                System.out.println("Jucatorul 4 a luat cartile");
            }
        } else {
            while (coadaPentrujoc.size() != 0) {
                coadaPentrujucator1.add(coadaPentrujoc.pool());
                n = 1;
                System.out.println("Jucatorul 1 a luat cartile");
            }
        }
        return n;
    }

    public void joc(int n, int primjucator) {
        try {
            validator.validate(n);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
        }
        impartire();
        Collections.shuffle(listadeCarti);
        if (n == 4) {    //if I have 4 players

             // divide the list in 4 queues for each player

            for (int i = 0; i < 6; i++) {
                j1.add(listadeCarti.get(i));
            }
            for (int i = 6; i < 12; i++) {
                j2.add(listadeCarti.get(i));
            }
            for (int i = 12; i < 18; i++) {
                j3.add(listadeCarti.get(i));
            }
            for (int i = 18; i < 24; i++) {
                j4.add(listadeCarti.get(i));
            }


            /**
             * i take one card from each player
             * remember they are hold in a priority queue which means the most valuable card it`s in the peek
             * i put them in another priority queue to see which one is the most valuable
             * and then i compare them to see which player had the winning card
             * if they are the same then the first player who had the most valuable card takes them
             */

            for (int i = 0; i < 6; i++) {

                System.out.println("tura" + i);
                System.out.println(j1.peek());
                System.out.println(j2.peek());
                System.out.println(j3.peek());
                System.out.println(j4.peek());

                Carte carte1 = j1.pool();
                Carte carte2 = j2.pool();
                Carte carte3 = j3.pool();
                Carte carte4 = j4.pool();

                coadaPentrujoc.add(carte1);
                coadaPentrujoc.add(carte2);
                coadaPentrujoc.add(carte3);
                coadaPentrujoc.add(carte4);

                Carte carteMax = coadaPentrujoc.peek();

                if (primjucator == 1) {

                    if (comparator.compare(carteMax, carte2) == 0 && comparator.compare(carte1, carte2) != 0) {
                        primjucator = decision(2);
                    } else if (comparator.compare(carteMax, carte3) == 0 && comparator.compare(carte1, carte3) != 0) {
                        primjucator = decision(3);
                    } else if (comparator.compare(carteMax, carte4) == 0 && comparator.compare(carte1, carte4) != 0) {
                        primjucator = decision(4);
                    } else {
                        primjucator = decision(1);
                    }

                } else if (primjucator == 2) {
                    if (comparator.compare(carteMax, carte1) == 0 && comparator.compare(carte1, carte2) != 0) {
                        primjucator = decision(1);
                    } else if (comparator.compare(carteMax, carte3) == 0 && comparator.compare(carte3, carte2) != 0) {
                        primjucator = decision(3);
                    } else if (comparator.compare(carteMax, carte4) == 0 && comparator.compare(carte4, carte2) != 0) {
                        primjucator = decision(4);
                    } else {
                        primjucator = decision(2);
                    }

                } else if (primjucator == 3) {
                    if (comparator.compare(carteMax, carte1) == 0 && comparator.compare(carte1, carte3) != 0) {
                        primjucator = decision(1);
                    } else if (comparator.compare(carteMax, carte2) == 0 && comparator.compare(carte2, carte3) != 0) {
                        primjucator = decision(2);
                    } else if (comparator.compare(carteMax, carte4) == 0 && comparator.compare(carte4, carte3) != 0) {
                        primjucator = decision(4);
                    } else {
                        primjucator = decision(3);
                    }

                } else if (primjucator == 4) {
                    if (comparator.compare(carteMax, carte1) == 0 && comparator.compare(carte1, carte4) != 0) {
                        primjucator = decision(1);
                    } else if (comparator.compare(carteMax, carte3) == 0 && comparator.compare(carte3, carte4) != 0) {
                        primjucator = decision(3);
                    } else if (comparator.compare(carteMax, carte2) == 0 && comparator.compare(carte2, carte4) != 0) {
                        primjucator = decision(2);
                    } else {
                        primjucator = decision(4);
                    }

                }

            }



             // calculate the sums for each player and compare them


            int sum1 = calculateWinner(coadaPentrujucator1);
            int sum2 = calculateWinner(coadaPentrujucator2);
            int sum3 = calculateWinner(coadaPentrujucator3);
            int sum4 = calculateWinner(coadaPentrujucator4);
            System.out.println(sum1 + " Jucator 1");
            System.out.println(sum2 + " Jucator 2");
            System.out.println(sum3 + " Jucator 3");
            System.out.println(sum4 + " Jucator 4");

            if (max(max(sum1, sum2), max(sum3, sum4)) == sum1) {
                System.out.println("A castigat jucatorul 1");
            } else if (max(max(sum1, sum2), max(sum3, sum4)) == sum2) {
                System.out.println("A castigat jucatorul 2");
            } else if (max(max(sum1, sum2), max(sum3, sum4)) == sum3) {
                System.out.println("A castigat jucatorul 3");
            } else {
                System.out.println("A castigat jucatorul 4");
            }

        } else if (n == 3) {
            Collections.shuffle(listadeCarti);

            for (int i = 0; i < 8; i++) {
                j1.add(listadeCarti.get(i));
            }
            for (int i = 8; i < 16; i++) {
                j2.add(listadeCarti.get(i));
            }
            for (int i = 16; i < 24; i++) {
                j3.add(listadeCarti.get(i));
            }


            for (int i = 0; i < 8; i++) {

                System.out.println("tura" + i);
                System.out.println(j1.peek());
                System.out.println(j2.peek());
                System.out.println(j3.peek());


                Carte carte1 = j1.pool();
                Carte carte2 = j2.pool();
                Carte carte3 = j3.pool();


                coadaPentrujoc.add(carte1);
                coadaPentrujoc.add(carte2);
                coadaPentrujoc.add(carte3);


                Carte carteMax = coadaPentrujoc.peek();

                if (primjucator == 1) {

                    if (comparator.compare(carteMax, carte2) == 0 && comparator.compare(carte1, carte2) != 0) {
                        primjucator = decision(2);
                    } else if (comparator.compare(carteMax, carte3) == 0 && comparator.compare(carte3, carte1) != 0) {
                        primjucator = decision(3);
                    } else {
                        primjucator = decision(1);
                    }

                } else if (primjucator == 2) {
                    if (comparator.compare(carteMax, carte1) == 0 && carte1 != carte2) {
                        primjucator = decision(1);
                    } else if (comparator.compare(carteMax, carte3) == 0 && carte3 != carte2) {
                        primjucator = decision(3);

                    } else {
                        primjucator = decision(2);
                    }

                } else if (primjucator == 3) {
                    if (comparator.compare(carteMax, carte1) == 0 && carte1 != carte3) {
                        primjucator = decision(1);
                    } else if (comparator.compare(carteMax, carte2) == 0 && carte2 != carte3) {
                        primjucator = decision(2);
                    } else {
                        primjucator = decision(3);
                    }

                }

            }


            int sum1 = calculateWinner(coadaPentrujucator1);
            int sum2 = calculateWinner(coadaPentrujucator2);
            int sum3 = calculateWinner(coadaPentrujucator3);
            System.out.println(sum1 + " Jucator 1");
            System.out.println(sum2 + " Jucator 2");
            System.out.println(sum3 + "Jucator 3");

            if (max(max(sum1, sum1), sum3) == sum1) {
                System.out.println("A castigat jucatorul 1");
            } else if (max(max(sum1, sum1), sum3) == sum2) {
                System.out.println("A castigat jucatorul 2");
            } else {
                System.out.println("A castigat jucatorul 3");

            }

        } else if (n == 2) {
            Collections.shuffle(listadeCarti);

            for (int i = 0; i < 12; i++) {
                j1.add(listadeCarti.get(i));
            }

            for (int i = 12; i < 24; i++) {
                j2.add(listadeCarti.get(i));
            }


            for (int i = 0; i < 12; i++) {

                System.out.println("tura" + i);
                System.out.println(j1.peek());
                System.out.println(j2.peek());


                //  i++;

                Carte carte1 = j1.pool();
                System.out.println("jucatorul 1 :" + carte1);
                Carte carte2 = j2.pool();
                System.out.println("jucatorul 2 :" + carte2);

                coadaPentrujoc.add(carte1);
                coadaPentrujoc.add(carte2);


                Carte carteMax = coadaPentrujoc.peek();

                if (primjucator == 1) {

                    if (comparator.compare(carteMax, carte2) == 0 && comparator.compare(carte1, carte2) != 0) {
                        primjucator = decision(2);

                    } else {
                        primjucator = decision(1);
                    }

                } else if (primjucator == 2) {
                    if (comparator.compare(carteMax, carte1) == 0 && comparator.compare(carte1, carte2) != 0) {
                        primjucator = decision(1);
                    } else {
                        primjucator = decision(2);
                    }

                }


            }

            int sum1 = calculateWinner(coadaPentrujucator1);
            int sum2 = calculateWinner(coadaPentrujucator2);
            System.out.println(sum1 + " Jucator 1");
            System.out.println(sum2 + " Jucator 2");
            if (max(sum1, sum2) == sum1) {
                System.out.println("A castigat jucatorul 1");
            } else {
                System.out.println("A castigat jucatorul 2");
            }


        }
    }

}


