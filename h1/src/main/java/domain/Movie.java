package domain;



public class Movie extends Entity<Long>{
     private String name;
     private int pret ;
     private String gen;

    public Movie() {
    }

    public Movie(String name, int pret, String gen) {
        super();
        this.name = name;
        this.pret = pret;
        this.gen = gen;

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", pret=" + pret +
                ", gen='" + gen + '\'' +
                ", id=" + id +
                '}';
    }
}
