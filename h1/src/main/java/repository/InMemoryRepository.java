package repository;

import domain.Entity;
import validators.Validator;
import validators.ValidatorException;

import java.util.*;
import java.util.stream.Collectors;


public class InMemoryRepository<ID, T extends Entity<ID>> implements Repository<ID, T> {

    private Map<ID, T> entities;
    private Validator<T> validator;

    public InMemoryRepository(Validator<T> validator) {
        this.validator = validator;
        entities = new HashMap<>();
    }

    @Override
    public Optional<T> findOne(ID id) {
        Optional<ID> op = Optional.ofNullable(id);
        op.orElseThrow(IllegalArgumentException::new);
        return Optional.ofNullable(entities.get(id));
    }


    @Override
    public Iterable<T> findAll() {
        Set<T> allEntities = entities.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toSet());
        return allEntities;
    }

    @Override
    public Optional<T> save(T entity) throws ValidatorException {
        Optional<T> op = Optional.ofNullable(entity);
        op.orElseThrow(() -> new IllegalArgumentException("entity must not be null"));
        validator.validate(entity);
        entities.putIfAbsent(entity.getId(), entity);

        return op;
    }

    @Override
    public Optional<T> delete(ID id) {
        Optional<ID> op = Optional.ofNullable(id);
        op.orElseThrow(IllegalArgumentException::new);
        return Optional.ofNullable(entities.remove(id));
    }

    @Override
    public Optional<T> update(T entity) throws ValidatorException {
        Optional<T> op = Optional.ofNullable(entity);
        op.orElseThrow(IllegalArgumentException::new);
        entities.computeIfPresent(entity.getId(), (k, v) -> entity);
        validator.validate(entity);
        return op;
    }


}
