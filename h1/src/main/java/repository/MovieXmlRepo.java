package repository;


import domain.Movie;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import validators.Validator;
import validators.ValidatorException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MovieXmlRepo extends InMemoryRepository<Long, Movie> {
    private String fileName;

    public MovieXmlRepo(Validator<Movie> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        load();


    }


    public Optional<Movie> delete(Long id) {
        Optional<Movie> optional = super.delete(id);
        deleteFromFile(id);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }


    public void load() {
        findAll().forEach(p -> super.save(p));
    }

    @Override
    public Optional<Movie> save(Movie entity) throws ValidatorException {

        if (StreamSupport.stream(super.findAll().spliterator(), false).count() == 0) {
            entity.setId((long) 1);

        } else {
            Movie movie = StreamSupport.stream(super.findAll().spliterator(), false)
                    .reduce((p1, p2) -> p1.getId() > p2.getId() ? p1 : p2)
                    .get();
            entity.setId(movie.getId() + 1);
        }

        Optional<Movie> optional = super.save(entity);


        saveTofile(entity);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }

    @Override
    public Optional<Movie> update(Movie movie) throws ValidatorException {

        Optional<Movie> optional = super.update(movie);
        super.findAll().forEach(p -> deleteFromFile(p.getId()));

        super.findAll().forEach(p -> saveTofile(p));

        if (optional.isPresent()) {
            return optional;
        }


        return Optional.empty();
    }


    private void deleteFromFile(Long ID) {

        try {
            Document document =
                    DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder()
                            .parse(fileName);


            NodeList list = document.getElementsByTagName("movie");

            for (int i = 0; i < list.getLength(); i++) {

                Element element = (Element) list.item(i);

                if (Long.parseLong(element.getElementsByTagName("ID").item(0).getTextContent()) == (ID)) {
                    element.getParentNode().removeChild(element);
                }
            }
            Transformer transformer =
                    TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(document),
                    new StreamResult(fileName));


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void saveTofile(Movie movie) {
        try {
            Document document =
                    DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder()
                            .parse(fileName);
            Element root = document.getDocumentElement();
            root.appendChild(createBookElement(document, movie));

            Transformer transformer =
                    TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(document),
                    new StreamResult(fileName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Node createBookElement(Document document, Movie movie) {
        Element MovieElement = document.createElement("movie");

        appendTagWithText(document, MovieElement, "ID",
                String.valueOf(movie.getId()));
        appendTagWithText(document, MovieElement, "name",
                movie.getName());
        appendTagWithText(document, MovieElement, "price",
                String.valueOf(movie.getPret()));
        appendTagWithText(document, MovieElement, "gen",
                movie.getGen());


        return MovieElement;
    }

    private void appendTagWithText(Document document, Element parent,
                                   String tagName, String textContent) {
        Element element = document.createElement(tagName);
        element.setTextContent(textContent);
        parent.appendChild(element);
    }

    @Override
    public List<Movie> findAll() {
        List<Movie> movies = new ArrayList<>();
        try {
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(this.fileName);
            Element root = document.getDocumentElement();
            NodeList MovieNodes = root.getChildNodes();
            for (int i = 0; i < MovieNodes.getLength(); i++) {
                Node movieNode = MovieNodes.item(i);
                if (movieNode instanceof Element) {
                    Movie movie = createBookFromElement((Element) movieNode);
                    movies.add(movie);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return movies;
    }

    private Movie createBookFromElement(Element MovieElement) {
        Movie movie = new Movie();


        String ID = getTextFromTag(MovieElement, "ID");
        movie.setId(Long.valueOf(ID));

        String name = getTextFromTag(MovieElement, "name");
        movie.setName(name);

        String price = getTextFromTag(MovieElement, "price");
        movie.setPret(Integer.parseInt(price != null ? price : "0"));

        String gen = getTextFromTag(MovieElement, "gen");
        movie.setGen(gen);

        return movie;
    }

    private static String getTextFromTag(Element movieElement, String tagName) {
        NodeList children = movieElement.getElementsByTagName(tagName);
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i) instanceof Element) {
                Element element = (Element) children.item(i);
                return element.getTextContent();
            }
        }
        return null;
    }


}
