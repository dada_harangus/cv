public class CoadaPeVector {


    private int capacitate;
    private int lungime;
    private Carte[] elemente;


    public CoadaPeVector(int capacitate) {
        this.capacitate = capacitate;
        this.lungime = 0;
        this.elemente = new Carte[capacitate];


    }


    public int getLungime() {
        return lungime;
    }


    Comparator.Comparatorcarti comparator = new Comparator.Comparatorcarti();

    public void add(Carte carte) {
        if (this.lungime == this.capacitate) {
            int capacitateNoua = 2 * this.capacitate;
            Carte[] elementeNou = new Carte[capacitateNoua];
            for (int i = 0; i < this.capacitate; i++) {
                elementeNou[i] = elemente[i];
            }
            this.elemente = elementeNou;
            this.capacitate = capacitateNoua;
        }

        this.elemente[this.lungime] = carte;
        lungime++;
    }

    public Carte peek() {
        Carte carteMax = this.elemente[0];
        for (int i = 0; i < this.lungime; i++) {
            Carte carte = this.elemente[i];
            if (comparator.compare(carte, carteMax) == -1) {
                carteMax = carte;
            }

        }
        return carteMax;
    }

    private void stergePozitie(int pozitie) {
        if (pozitie >= 0 && pozitie < this.lungime) {

            for (int i = pozitie; i < this.lungime - 1; i++) {
                this.elemente[i] = this.elemente[i + 1];
            }
            this.lungime--;
        }
    }

    public void stergeElem(Carte carte) {
        int index = 0;
        boolean gasit = false;
        while (index < this.lungime && gasit == false) {
            if (this.elemente[index] == carte) {
                gasit = true;
                stergePozitie(index);
            } else {
                index++;
            }
        }
    }


    public Carte pool() {

        Carte carte = new Carte(peek().getCuloare(), peek().getNumar());
        stergeElem(peek());
        return carte;


    }

    public void clear() {
        this.lungime = lungime - lungime;
    }

    public void addAll(CoadaPeVector coadaPeVector, CoadaPeVector coadaPeVector1) {

        for (int a = 0; a < coadaPeVector.getLungime(); a++) {
            coadaPeVector1.add(coadaPeVector.elemente[a]);
        }

    }

    public int size() {
        int size = this.lungime;
        return size;
    }
}
