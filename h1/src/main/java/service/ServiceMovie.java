package service;

import domain.Inchirieri;
import domain.Movie;
import domain.Rentals;

import repository.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceMovie {


    private Repository<Long, Movie> repositoryMovie;
    private Repository<Inchirieri, Rentals> rentalsRepository;

    public ServiceMovie(Repository<Long, Movie> repositoryMovie, Repository<Inchirieri, Rentals> rentalsRepository) {
        this.repositoryMovie = repositoryMovie;
        this.rentalsRepository = rentalsRepository;
    }

    public void saveMovie(Movie movie) {


        repositoryMovie.save(movie);
    }


    public List<Movie> searchByPrice(int price1, int price2) {


        List<Movie> movies = StreamSupport.stream(repositoryMovie.findAll().spliterator(), false)
                .filter(movie -> movie.getPret() > price1 && movie.getPret() < price2)
                .collect(Collectors.toList());


        return movies;

    }


    public Set<Movie> getAllMovies() {
        Iterable<Movie> movies = repositoryMovie.findAll();
        return StreamSupport.stream(movies.spliterator(), false).collect(Collectors.toSet());
    }

    public void update(String numeVechie, Movie movie) {


        List<Movie> list = StreamSupport.stream(repositoryMovie.findAll().spliterator(), false)
                .filter(movie1 -> movie1.getName().equals(numeVechie))
                .collect(Collectors.toList());

        //Optional<Long> optionalInteger = Optional.ofNullable(list.stream().count());
        //optionalInteger.orElseThrow(() -> new IllegalArgumentException("movie not found"));

        if (list.stream().count() == 0) {
            throw new IllegalArgumentException("movie not found");
        }
        Long id = list.get(0).getId();
        movie.setId(id);
        repositoryMovie.update(movie);

    }


    public void delete(String nume) {


        List<Movie> list = StreamSupport.stream(repositoryMovie.findAll().spliterator(), false)
                .filter(movie1 -> movie1.getName().equals(nume))
                .collect(Collectors.toList());

        // Optional<Long> optionalInteger = Optional.ofNullable(list.stream().count());
        //optionalInteger.orElseThrow(() -> new IllegalArgumentException("movie not found"));
        if (list.stream().count() == 0) {
            throw new IllegalArgumentException("movie not found");
        }

        List<Rentals> list1 = StreamSupport.stream(rentalsRepository.findAll().spliterator(), false)
                .filter(rentals -> rentals.getId().getIdMovie().equals(list.get(0).getId().toString()))
                .collect(Collectors.toList());
        rentalsRepository.delete(list1.get(0).getId());
        repositoryMovie.delete(list.get(0).getId());

    }

    public Movie findOne(String name) {
        List<Movie> list = StreamSupport.stream(repositoryMovie.findAll().spliterator(), false)
                .filter(movie -> movie.getName().contains(name))
                .collect(Collectors.toList());
        if (list.stream().count() == 0) {
            throw new IllegalArgumentException("movie not found");
        }

        return list.get(0);
    }

}
