package repository;

import domain.Client;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import validators.Validator;
import validators.ValidatorException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

public class ClientXmlrepo extends InMemoryRepository<Long, Client> {
    private String fileName;

    public ClientXmlrepo(Validator<Client> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        load();
    }

    public void load() {
        findAll().forEach(p -> super.save(p));
    }


    public Optional<Client> delete(Long id) {
        Optional<Client> optional = super.delete(id);
        deleteFromFile(id);
        if (optional.isPresent()) {
            return optional;
        }
        return Optional.empty();
    }


    private void deleteFromFile(Long ID) {

        try {
            Document document =
                    DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder()
                            .parse(fileName);


            NodeList list = document.getElementsByTagName("oamenii");

            for (int i = 0; i < list.getLength(); i++) {

                Element element = (Element) list.item(i);

                if (Long.parseLong(element.getElementsByTagName("ID").item(0).getTextContent()) == (ID)) {
                    element.getParentNode().removeChild(element);
                }
            }
            Transformer transformer =
                    TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(document),
                    new StreamResult(fileName));


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public Optional<Client> save(Client entity) throws ValidatorException {
        if (StreamSupport.stream(super.findAll().spliterator(), false).count() == 0) {
            entity.setId((long) 1);

        } else {
            Client client = StreamSupport.stream(super.findAll().spliterator(), false)
                    .reduce((p1, p2) -> p1.getId() > p2.getId() ? p1 : p2)
                    .get();
            entity.setId(client.getId() + 1);
        }

        Optional<Client> optional = super.save(entity);
        saveClientTofile(entity);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }

    @Override
    public Optional<Client> update(Client movie) throws ValidatorException {
        Optional<Client> optional = super.update(movie);

        super.findAll().forEach(p -> deleteFromFile(p.getId()));

        super.findAll().forEach(p -> saveClientTofile(p));
        if (optional.isPresent()) {
            return optional;
        }
        return Optional.empty();
    }

    private void saveClientTofile(Client client) {
        try {
            Document doc =
                    DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder()
                            .parse(fileName);
            Element root = doc.getDocumentElement();
            root.appendChild(createClientElement(doc, client));

            Transformer transformer =
                    TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(doc),
                    new StreamResult(fileName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Node createClientElement(Document document, Client client) {
        Element ClientElement = document.createElement("oamenii");

        appendTagWithText(document, ClientElement, "ID",
                String.valueOf(client.getId()));
        appendTagWithText(document, ClientElement, "name",
                client.getName());
        appendTagWithText(document, ClientElement, "age",
                String.valueOf(client.getAge()));
        return ClientElement;
    }

    private void appendTagWithText(Document document, Element parent,
                                   String tagName, String textContent) {
        Element element = document.createElement(tagName);
        element.setTextContent(textContent);
        parent.appendChild(element);
    }

    @Override
    public List<Client> findAll() {
        List<Client> clients = new ArrayList<>();
        try {
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(this.fileName);
            Element root = document.getDocumentElement();
            NodeList ClientNodes = root.getChildNodes();
            for (int i = 0; i < ClientNodes.getLength(); i++) {
                Node clientNode = ClientNodes.item(i);
                if (clientNode instanceof Element) {
                    Client client = createClientFromElement((Element) clientNode);
                    clients.add(client);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clients;
    }

    private Client createClientFromElement(Element ClientElement) {
        Client client = new Client();

        String ID = getTextFromTag(ClientElement, "ID");
        client.setId(Long.valueOf(ID));

        String name = getTextFromTag(ClientElement, "name");
        client.setName(name);

        String age = getTextFromTag(ClientElement, "age");
        client.setAge(Integer.parseInt(age != null ? age : "0"));


        return client;
    }

    private static String getTextFromTag(Element clientElement, String tagName) {
        NodeList children = clientElement.getElementsByTagName(tagName);
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i) instanceof Element) {
                Element element = (Element) children.item(i);
                return element.getTextContent();
            }
        }
        return null;
    }
}
