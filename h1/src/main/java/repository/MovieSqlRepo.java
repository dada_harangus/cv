package repository;

import domain.Movie;
import validators.Validator;
import validators.ValidatorException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MovieSqlRepo extends InMemoryRepository<Long, Movie> {


    private static final String URL = "jdbc:postgresql://localhost:5432" +
            "/movies";
    private static final String USERNAME = System.getProperty("user");
    private static final String PASSWORD = System.getProperty("password");

    public MovieSqlRepo(Validator validator) {
        super(validator);
    }


    public Optional<Movie> save(Movie entity) throws ValidatorException {
        Optional<Movie> optional = super.save(entity);
        saveIntoDb(entity);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }


    public Optional<Movie> delete(Long id) {
        Optional<Movie> optional = super.delete(id);
        deleteFromDB(id);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }

    @Override
    public Optional<Movie> update(Movie movie) throws ValidatorException {
        Optional<Movie> optional = super.update(movie);
        updateDB(movie);
        if (optional.isPresent()) {
            return optional;
        }


        return Optional.empty();
    }

    public List<Movie> findAll() {
        List<Movie> movies = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "select * from movies");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                int price = resultSet.getInt("price");
                String gen = resultSet.getString("gen");

                Movie movie = new Movie();
                movie.setId(id);
                movie.setName(name);
                movie.setPret(price);
                movie.setGen(gen);

                movies.add(movie);
            }

        } catch (SQLException e) {


        }
        return movies;
    }

    private void saveIntoDb(Movie movie) {

        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "insert into movies(name, price, gen) " +
                             "values (?,?,?)");
        ) {
            statement.setString(1, movie.getName());
            statement.setInt(2, movie.getPret());
            statement.setString(3, movie.getGen());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    private void updateDB(Movie movie) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "update movies set name = ?,price=?," +
                             "gen=?  where id= ?")
        ) {
            statement.setString(1, movie.getName());
            statement.setLong(4, movie.getId());

            statement.setInt(2, movie.getPret());
            statement.setString(3, movie.getGen());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void deleteFromDB(Long id) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME,
                PASSWORD);
             PreparedStatement statement = connection.prepareStatement(
                     "delete from movies where id = ?")
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
