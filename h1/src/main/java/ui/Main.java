package ui;

import domain.Client;
import domain.Inchirieri;
import domain.Movie;
import domain.Rentals;
import repository.Repository;
import repository.*;
import service.ServiceClient;
import service.ServiceMovie;
import service.ServiceRental;
import validators.*;

public class Main {
    public static void main(String[] args) {
        Validator<Client> ValidatorClient = new ClientValidator();
        Validator<Movie> ValidatorMovie = new MovieValidator();
        Validator<Rentals> ValidatorRentals=new RentalsValidator();

        Repository<Long, Movie> MovieXmlRepo = new MovieXmlRepo(ValidatorMovie,"C:\\Users\\Dada\\IdeaProjects\\laborator1_hello\\h1\\data\\movie.xml");
        Repository<Long, Client> ClientXmlRepo= new ClientXmlrepo(ValidatorClient,"C:\\Users\\Dada\\IdeaProjects\\laborator1_hello\\h1\\data\\client.xml");
        Repository<Inchirieri, Rentals> rentalsRepositoryXml=new RentalsXmlRepo(ValidatorRentals,"C:\\Users\\Dada\\IdeaProjects\\laborator1_hello\\h1\\data\\rental.xml");


        Repository<Long ,Client> ClientRepositorySql=new ClientSqlRepo(ValidatorClient);
        Repository <Long,Movie> movieRepositorySql=new MovieSqlRepo(ValidatorMovie);
        Repository<Inchirieri,Rentals> rentalsRepositorySwl=new RentalsRepoSql(ValidatorRentals);

        Repository<Long, Movie> MovieFileRepo = new MovieRepo(ValidatorMovie,"C:\\Users\\Dada\\IdeaProjects\\laborator1_hello\\h1\\data\\movie.txt");
        Repository<Long, Client> ClientFileRepo= new ClientRepo(ValidatorClient,"C:\\Users\\Dada\\IdeaProjects\\laborator1_hello\\h1\\data\\client.txt");
        Repository<Inchirieri, Rentals> rentalsFileRepo=new RentalsFileRepo(ValidatorRentals,"C:\\Users\\Dada\\IdeaProjects\\laborator1_hello\\h1\\data\\rental.txt");




        ServiceClient serviceClient = new ServiceClient(ClientFileRepo,rentalsFileRepo);
        ServiceMovie serviceMovie=new ServiceMovie(MovieFileRepo,rentalsFileRepo);
        ServiceRental serviceRental=new ServiceRental(rentalsFileRepo,ClientFileRepo,MovieFileRepo);

        Consola consola=new Consola(serviceMovie, serviceClient,serviceRental);
        consola.runMenu();
    }
}
