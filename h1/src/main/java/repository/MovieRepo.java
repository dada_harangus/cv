package repository;

import domain.Client;
import domain.Movie;
import domain.Inchirieri;
import validators.Validator;
import validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


public class MovieRepo extends InMemoryRepository<Long, Movie> {

    private String movie;

    public MovieRepo(Validator<Movie> validator, String fileName) {
        super(validator);
        this.movie = fileName;

        loadData();
    }

    private void loadData() {
        Path path = Paths.get(movie);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long id = Long.valueOf(items.get(3));
                String nume = items.get(0);
                int pret = Integer.parseInt(items.get((1)));
                String gen = items.get(2);

                Movie movie = new Movie(nume, pret, gen);
                movie.setId(id);

                try {
                    super.save(movie);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public Optional<Movie> save(Movie entity) throws ValidatorException {
        if (StreamSupport.stream(super.findAll().spliterator(), false).count() == 0) {
            entity.setId((long) 1);

        } else {
            Movie movie = StreamSupport.stream(super.findAll().spliterator(), false)
                    .reduce((p1, p2) -> p1.getId() > p2.getId() ? p1 : p2)
                    .get();
            entity.setId(movie.getId() + 1);
        }
        Optional<Movie> optional = super.save(entity);
        saveToFile(entity);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }


    private void rewriteFile() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(movie))) {
            for (Movie entity : super.findAll()) {
                writer.write(entity.getName() + "," + entity.getPret() + "," + entity.getGen() + "," + entity.getId());
                writer.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Optional<Movie> update(Movie movie1) throws ValidatorException {

        Optional<Movie> optional = super.update(movie1);

        rewriteFile();


        if (optional.isPresent()) {
            return optional;
        }


        return Optional.empty();
    }


    @Override

    public Optional<Movie> delete(Long id) {


        Optional<Movie> optional = super.delete(id);

        rewriteFile();


        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }

    private void saveToFile(Movie entity) {
        Path path = Paths.get(movie);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(entity.getName() + "," + entity.getPret() + "," + entity.getGen() + "," + entity.getId());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
