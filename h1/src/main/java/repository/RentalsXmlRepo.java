package repository;

import domain.Inchirieri;
import domain.Rentals;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import validators.Validator;
import validators.ValidatorException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


//if din functia filtru pentru ca lucrez cu 3 colectii
//if din functiile unde returneaza un optional daca o reusit sau unul gol daca nu


public class RentalsXmlRepo extends InMemoryRepository<Inchirieri, Rentals> {
    private String fileName;


    public RentalsXmlRepo(Validator<Rentals> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        load();
    }


    public Optional<Rentals> delete(Inchirieri inchirieri) {
        Optional<Rentals> optional = super.delete(inchirieri);
        deleteFromFile(inchirieri);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }


    private void load() {
        findAll().forEach(p -> super.save(p));
    }

//
//    @Override
//    public Optional<Rentals> findOne(Inchirieri inchirieri) {
//        return super.findOne(inchirieri);
//    }

    @Override
    public Optional<Rentals> save(Rentals rentals) throws ValidatorException {
        Optional<Rentals> optional = super.save(rentals);
        saveTofile(rentals);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }


    private void deleteFromFile(Inchirieri inchirieri) {

        try {
            Document document =
                    DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder()
                            .parse(fileName);


            NodeList list = document.getElementsByTagName("rental");

            for (int i = 0; i < list.getLength(); i++) {

                Element element = (Element) list.item(i);

                if (element.getElementsByTagName("idClient").item(0).getTextContent() == inchirieri.getIdClient() && element.getElementsByTagName("idMovie").item(0).getTextContent() == inchirieri.getIdMovie()) {
                    element.getParentNode().removeChild(element);
                }
            }
            Transformer transformer =
                    TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(document),
                    new StreamResult(fileName));


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void saveTofile(Rentals rental) {
        try {
            Document document =
                    DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder()
                            .parse(fileName);
            Element root = document.getDocumentElement();
            root.appendChild(createBookElement(document, rental));

            Transformer transformer =
                    TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(document),
                    new StreamResult(fileName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Node createBookElement(Document document, Rentals rentals) {
        Element RentalElement = document.createElement("rental");

        appendTagWithText(document, RentalElement, "idClient",
                rentals.getId().getIdClient());
        appendTagWithText(document, RentalElement, "idMovie",
                rentals.getId().getIdMovie());


        return RentalElement;
    }

    private void appendTagWithText(Document document, Element parent,
                                   String tagName, String textContent) {
        Element element = document.createElement(tagName);
        element.setTextContent(textContent);
        parent.appendChild(element);
    }

    @Override
    public List<Rentals> findAll() {
        List<Rentals> rentals = new ArrayList<>();
        try {
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(this.fileName);
            Element root = document.getDocumentElement();
            NodeList RentalsNodes = root.getChildNodes();
            for (int i = 0; i < RentalsNodes.getLength(); i++) {
                Node rentalNode = RentalsNodes.item(i);
                if (rentalNode instanceof Element) {
                    Rentals rentals1 = createRentalFromElement((Element) rentalNode);
                    rentals.add(rentals1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rentals;
    }

    private Rentals createRentalFromElement(Element rentalElement) {
        Rentals rentals = new Rentals();
        Inchirieri inchirieri = new Inchirieri();
        String idClient = getTextFromTag(rentalElement, "idClient");
        inchirieri.setIdClient(idClient);
        String idMovie = getTextFromTag(rentalElement, "idMovie");
        inchirieri.setIdMovie(idMovie);
        rentals.setId(inchirieri);


        return rentals;
    }

    private static String getTextFromTag(Element rentalElement, String tagName) {
        NodeList children = rentalElement.getElementsByTagName(tagName);
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i) instanceof Element) {
                Element element = (Element) children.item(i);
                return element.getTextContent();
            }
        }
        return null;
    }
}
