package repository;

import domain.Client;
import domain.Inchirieri;
import domain.Movie;
import domain.Rentals;
import validators.Validator;
import validators.ValidatorException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class RentalsFileRepo extends InMemoryRepository<Inchirieri, Rentals> {

    private String rental;


    public RentalsFileRepo(Validator<Rentals> validator, String fileName) {

        super(validator);
        this.rental = fileName;

        findAll();
    }

    @Override
    public List<Rentals> findAll() {
        Path path = Paths.get(rental);
        List<Rentals> LIST = new ArrayList<>();
        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                String idclient = items.get(0);
                String idmovie = items.get(1);

                Inchirieri inchirieri = new Inchirieri(idclient, idmovie);

                Rentals rentals = new Rentals();
                rentals.setId(inchirieri);

                try {
                    super.save(rentals);
                    LIST.add(rentals);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return LIST;
    }


    @Override
    public Optional<Rentals> save(Rentals entity) throws ValidatorException {
        Optional<Rentals> optional = super.save(entity);
        saveToFile(entity);
        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }


    private void saveToFile(Rentals entity) {
        Path path = Paths.get(rental);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(entity.getId().getIdClient() + "," + entity.getId().getIdMovie());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void rewriteFile() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(rental))) {
            for (Rentals entity : super.findAll()) {
                writer.write(entity.getId().getIdClient() + "," + entity.getId().getIdMovie());
                writer.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Optional<Rentals> update(Rentals rentals) throws ValidatorException {

        Optional<Rentals> optional = super.update(rentals);

        rewriteFile();


        if (optional.isPresent()) {
            return optional;
        }


        return Optional.empty();
    }


    @Override

    public Optional<Rentals> delete(Inchirieri inchirieri) {


        Optional<Rentals> optional = super.delete(inchirieri);

        rewriteFile();


        if (optional.isPresent()) {
            return optional;
        }

        return Optional.empty();
    }


}
