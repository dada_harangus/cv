package validators;

import domain.Movie;

import java.util.Optional;

public class MovieValidator implements Validator<Movie> {


    @Override
    public void validate(Movie entity) throws ValidatorException {
        Optional<Movie> op = Optional.ofNullable(entity);

        op = op.filter(movie -> movie.getPret() < 1000);
        op = op.filter(movie -> movie.getPret() > 0);
     //   op.orElseThrow(() -> new ValidatorException("The price is out of bound"));


    }
}
