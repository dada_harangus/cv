<html>
<?php session_start(); ?>
<?php include("head.php");?>
  <style>.error {color:#FF0000;}</style>
<body>
<?php
$passworderr1=$passworderr2="";
$eroare=0;
require_once ("config.php");
// functie pentru sanitizarea datelor primite din formular
function test_input($data)
  {
    $data=trim($data);
    $data=stripslashes($data);
    $data=htmlspecialchars($data);
    return $data;
  }
// verifica daca au fost trimise date din formular
if ($_SERVER["REQUEST_METHOD"]=="POST")
  {
    if (empty($_POST["parolaa"]))
      {
        $passworderr1="Parola este obligatorie";
        $eroare=1;
      }
    else
      {
        if (empty($_POST["parolab"]))
          {
            $passworderr1="Verificare parola este obligatorie";
            $eroare=1;
          }
          else
            {
// se verefica daca cele doua parole sunt identice
              if ($_POST["parolaa"]!=$_POST["parolab"])
                {
                  $passworderr1="Parola nu se potriveste";
                  $eroare=1;
                }
            }
      }
      if (empty($_POST["parolav"]))
        {
          $passworderr2="Parola veche este obligatorie";
          $eroare=1;
        }
      else
        {
          $parolaveche=md5($_POST["parolav"]);
          if ($parolaveche!=$_SESSION['UserParola'])
            {
              $passworderr2="Parola veche nu se potriveste";
              $eroare=1;
            }
        }
        if ($eroare==0)
        {
          if ($_POST["parolav"]==$_POST["parolaa"])
          {
            $passworderr1="Parola noua nu poate fi identic cu cel vechi";
            $eroare=1;
          }
        }
        if ($eroare==0)
        {
          $parolanoua=md5($_POST["parolaa"]);
          $iduser=$_SESSION['UserID'];
          require_once ("config.php");
          $db=mysqli_connect($host,$usernamesql,$passwordsql,$db_name) or die ("could not connect");
          $query="UPDATE $tbl_name SET parola = '$parolanoua' WHERE id='$iduser'";
          $result=mysqli_query($db,$query);
          $_SESSION['UserParola'] = $parolanoua;
          header("Location: index.php");
        }
  }
?>
  <h2>Schimbare parola</h2>
  <p><span class="error">* camp obligatoriu</span></p>
  <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" target="_self" enctype="multipart/form-data">
    <fieldset>
    <legend>Schimbare parola</legend>
      Parola veche:  <br><input type="password" name="parolav" lenght="40">
      <span class="error">* <?php echo $passworderr2;?></span><br><br>
      Parola: <br><input type="password" name="parolaa" lenght="40">
      <br><br>
      Verificare parola: <br><input type="password" name="parolab" lenght="40">
      <span class="error">* <?php echo $passworderr1;?></span><br>
    </fieldset>
    <br>
      <input type="submit" name="submit" value="Trimite">
  </form>
  <br><br>
  <a href='index.php' target='_self'>Prima pagina</a>
  &nbsp; | &nbsp;
  <a href='logout.php' target='_self'>Logout</a>
  </body></html>
